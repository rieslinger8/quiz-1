// Nomor 1

var tomorrow = new Date();
tomorrow.setDate(tomorrow.getDate() + 1)

console.log(tomorrow);


// Nomor 2 

function jumlah_kata(str) {
    str = str.replace(/(^\s*)|(\s*$)/gi, "");
    str = str.replace(/[ ]{2,}/gi, " ");
    str = str.replace(/\n /, "\n");
    return str.split(' ').length;
}

console.log(jumlah_kata('Halo nama saya Muhammad Iqbal Mubarok '));